﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_Look_Return : MonoBehaviour {

	public Transform objectToLookAt;
	public float timeToStare;
	public float timeToLook;
	public float timeToReturn;
	public Camera_Look cameraLook;
	bool done = false;
	bool returned = true;
	Quaternion lookOnLook;

	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (done == false) 
		{
			StartCoroutine( FocusOnObject ());
		}
		else if(returned == false)
			ReturnToFree ();
	}

	IEnumerator FocusOnObject()
	{
		lookOnLook = Quaternion.LookRotation (objectToLookAt.position - cameraLook.transform.position);
		if (Quaternion.Angle (cameraLook.transform.rotation, lookOnLook) >= 2) 
		{
			cameraLook.transform.rotation = Quaternion.Slerp (cameraLook.transform.rotation, lookOnLook, Time.deltaTime * (1 / timeToStare));
			cameraLook.isLocked = true;
		} 
		else 
		{
			yield return new WaitForSeconds (timeToLook);
			done = true;
			returned = false;


		}
	}

	void ReturnToFree()
	{
		Quaternion returnLook = Quaternion.LookRotation (cameraLook.transform.position - objectToLookAt.position);
		Quaternion finalLook = Quaternion.Slerp (lookOnLook, returnLook, 0.4f);
		if (Quaternion.Angle (finalLook, cameraLook.transform.rotation) >= 2) 
		{
			cameraLook.transform.rotation = Quaternion.Slerp ( cameraLook.transform.rotation, finalLook, Time.deltaTime * (1/timeToReturn));
			cameraLook.isLocked = true;
		} 
		else 
		{
			cameraLook.isLocked = false;
			returned = true;
		}
	}
}
