﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Camera_Fade : MonoBehaviour {

	public float timeOfFade;
	private float timeFaded;
	private float alphaLevel;
	public Image fadeUI;

	// Use this for initialization
	void Start (){
		timeFaded = 0;
	}

	// Update is called once per frame
	void Update () {
		CameraSplatter ();
	}

	public void CameraSplatter(){
		if (timeFaded < timeOfFade) {
			alphaLevel = timeFaded / timeOfFade;
			fadeUI.color = new Color(fadeUI.color.r, fadeUI.color.g, fadeUI.color.b, alphaLevel);
			timeFaded += (1 * Time.deltaTime);
		} else if (timeFaded >= timeOfFade) {
			timeFaded = timeOfFade;
			fadeUI.color = new Color(fadeUI.color.r, fadeUI.color.g, fadeUI.color.b, alphaLevel);
		}
	}
}
